package com.semenov.foodApplication.dto;


import lombok.*;

import java.util.Date;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Data
public class UserDTO {
    private Integer id;
    private String userName;
    private String password;
    private String fullName;
    private Date createDate;
}
