package com.semenov.foodApplication.entity.key;

import jakarta.persistence.Column;
import jakarta.persistence.Embeddable;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.proxy.HibernateProxy;

import java.io.Serializable;
import java.util.Objects;

@Embeddable
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class KeyMenuRestaurant implements Serializable {
    @Column(name = "cate_id")
    private Integer cateId;

    @Column(name = "res_id")
    private Integer resId;

    @Override
    public final boolean equals(Object o) {
        if (this == o) return true;
        if (o == null) return false;
        Class<?> oEffectiveClass = o instanceof HibernateProxy ? ((HibernateProxy) o).getHibernateLazyInitializer().getPersistentClass() : o.getClass();
        Class<?> thisEffectiveClass = this instanceof HibernateProxy ? ((HibernateProxy) this).getHibernateLazyInitializer().getPersistentClass() : this.getClass();
        if (thisEffectiveClass != oEffectiveClass) return false;
        KeyMenuRestaurant that = (KeyMenuRestaurant) o;
        return getCateId() != null && Objects.equals(getCateId(), that.getCateId())
                && getResId() != null && Objects.equals(getResId(), that.getResId());
    }

    @Override
    public final int hashCode() {
        return Objects.hash(cateId, resId);
    }
}
