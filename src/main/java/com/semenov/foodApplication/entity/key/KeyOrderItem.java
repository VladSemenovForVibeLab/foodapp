package com.semenov.foodApplication.entity.key;

import jakarta.persistence.Column;
import jakarta.persistence.Embeddable;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.proxy.HibernateProxy;

import java.io.Serializable;
import java.util.Objects;

@Embeddable
@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
public class KeyOrderItem implements Serializable {
    @Column(name = "order_id")
    private Integer orderId;

    @Column(name = "food_id")
    private Integer foodId;

    @Override
    public final boolean equals(Object o) {
        if (this == o) return true;
        if (o == null) return false;
        Class<?> oEffectiveClass = o instanceof HibernateProxy ? ((HibernateProxy) o).getHibernateLazyInitializer().getPersistentClass() : o.getClass();
        Class<?> thisEffectiveClass = this instanceof HibernateProxy ? ((HibernateProxy) this).getHibernateLazyInitializer().getPersistentClass() : this.getClass();
        if (thisEffectiveClass != oEffectiveClass) return false;
        KeyOrderItem that = (KeyOrderItem) o;
        return getOrderId() != null && Objects.equals(getOrderId(), that.getOrderId())
                && getFoodId() != null && Objects.equals(getFoodId(), that.getFoodId());
    }

    @Override
    public final int hashCode() {
        return Objects.hash(orderId, foodId);
    }
}
