package com.semenov.foodApplication.repository;

import com.semenov.foodApplication.entity.RatingRestaurant;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RatingRestaurantRepository extends JpaRepository<RatingRestaurant, Integer> {
}