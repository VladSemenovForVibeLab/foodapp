package com.semenov.foodApplication.repository;

import com.semenov.foodApplication.entity.OrderItem;
import com.semenov.foodApplication.entity.key.KeyOrderItem;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface OrderItemRepository extends JpaRepository<OrderItem, KeyOrderItem> {
}