package com.semenov.foodApplication.repository;

import com.semenov.foodApplication.entity.MenuRestaurant;
import com.semenov.foodApplication.entity.key.KeyMenuRestaurant;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MenuRestaurantRepository extends JpaRepository<MenuRestaurant, KeyMenuRestaurant> {
}