package com.semenov.foodApplication.repository;

import com.semenov.foodApplication.entity.RatingFood;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RatingFoodRepository extends JpaRepository<RatingFood, Integer> {
}