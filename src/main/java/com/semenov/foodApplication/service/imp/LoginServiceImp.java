package com.semenov.foodApplication.service.imp;

import com.semenov.foodApplication.dto.UserDTO;
import com.semenov.foodApplication.entity.Users;
import com.semenov.foodApplication.payload.request.SignUpRequest;

import java.util.List;

public interface LoginServiceImp {
    List<UserDTO> getAllUsers();
    boolean checkLogin(String username, String password);

    boolean addUser(SignUpRequest signUpRequest);
}
