package com.semenov.foodApplication.service;

import com.semenov.foodApplication.dto.UserDTO;
import com.semenov.foodApplication.entity.Roles;
import com.semenov.foodApplication.entity.Users;
import com.semenov.foodApplication.payload.request.SignUpRequest;
import com.semenov.foodApplication.repository.UsersRepository;
import com.semenov.foodApplication.service.imp.LoginServiceImp;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class LoginService implements LoginServiceImp {
    private final UsersRepository usersRepository;
    @Override
    public List<UserDTO> getAllUsers(){
        List<Users> listUser = usersRepository.findAll();
        List<UserDTO> userDTOList = new ArrayList<>();
        for(Users user: listUser){
            UserDTO userDTO = new UserDTO();
            userDTO.setId(user.getId());
            userDTO.setFullName(user.getFullName());
            userDTO.setUserName(user.getUserName());
            userDTO.setCreateDate(user.getCreateDate());
            userDTOList.add(userDTO);
        }
        return userDTOList;
    }

    @Override
    public boolean checkLogin(String username, String password) {
        List <Users> listUsers = usersRepository.findByUserNameAndPassword(username,password);
        return !listUsers.isEmpty();
    }

    @Override
    public boolean addUser(SignUpRequest signUpRequest) {
        Users user = new Users();
        Roles role = new Roles();
        role.setId(signUpRequest.getRoleId());
        user.setFullName(signUpRequest.getFullName());
        user.setUserName(signUpRequest.getEmail());
        user.setPassword(signUpRequest.getPassword());
        user.setRole(role);
        try {
            usersRepository.save(user);
            return true;
        }catch (Exception e){
            return false;
        }
    }
}
