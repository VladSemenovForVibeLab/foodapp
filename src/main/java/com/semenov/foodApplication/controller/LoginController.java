package com.semenov.foodApplication.controller;

import com.semenov.foodApplication.payload.ResponseData;
import com.semenov.foodApplication.payload.request.SignUpRequest;
import com.semenov.foodApplication.service.LoginService;
import com.semenov.foodApplication.service.imp.LoginServiceImp;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/login")
@RequiredArgsConstructor
public class LoginController {
    private final LoginServiceImp loginServiceImp;
    @PostMapping("/signin")
    public ResponseEntity<?> signin(@RequestParam String username,@RequestParam String password) {
        ResponseData responseData = new ResponseData();
        if(loginServiceImp.checkLogin(username,password)){
            responseData.setData(true);
        }else{
            responseData.setData(false);
        }
        return new ResponseEntity<>(responseData, HttpStatus.OK);
    }

    @PostMapping("/signup")
    public ResponseEntity<?> signup(@RequestBody SignUpRequest signUpRequest){
        ResponseData responseData = new ResponseData();
        responseData.setData(loginServiceImp.addUser(signUpRequest));
        return new ResponseEntity<>(responseData,HttpStatus.OK);
    }
}
