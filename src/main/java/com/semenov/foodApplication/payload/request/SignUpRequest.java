package com.semenov.foodApplication.payload.request;

import lombok.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
@EqualsAndHashCode
public class SignUpRequest {
    private String fullName;
    private String email;
    private String password;
    private int roleId;
}
