package com.semenov.foodApplication.payload;

import lombok.*;

@Getter
@Setter
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ResponseData {
    private int status = 200;
    private String description;
    private Object data;
}
