
CREATE TABLE roles (
                       id SERIAL PRIMARY KEY,
                       role_name VARCHAR(20),
                       create_date TIMESTAMP
);
CREATE TABLE category (
                          id SERIAL PRIMARY KEY,
                          name_cate VARCHAR(100),
                          create_date TIMESTAMP
);

CREATE TABLE users (
                       id SERIAL PRIMARY KEY,
                       user_name VARCHAR(100),
                       password VARCHAR(100),
                       full_name VARCHAR(50),
                       create_date TIMESTAMP,
                       role_id INT,
                       FOREIGN KEY (role_id) REFERENCES roles(id)
);
CREATE TABLE food (
                      id SERIAL PRIMARY KEY,
                      title VARCHAR(255),
                      image TEXT,
                      time_ship VARCHAR(10),
                      price NUMERIC,
                      cate_id INT,
                      FOREIGN KEY (cate_id) REFERENCES category(id)
);



CREATE TABLE rating_food (
                            id SERIAL PRIMARY KEY,
                            user_id INT,
                            food_id INT,
                            content TEXT,
                            rate_point INT,
                            FOREIGN KEY (user_id) REFERENCES users(id),
                            FOREIGN KEY (food_id) REFERENCES food(id)
);

CREATE TABLE restaurant (
                            id SERIAL PRIMARY KEY,
                            title VARCHAR(255),
                            subtitle VARCHAR(255),
                            description TEXT,
                            image TEXT,
                            is_freeship BOOLEAN,
                            address VARCHAR(255),
                            open_date TIMESTAMP
);

CREATE TABLE rating_restaurant (
                                  id SERIAL PRIMARY KEY,
                                  user_id INT,
                                  res_id INT,
                                  content TEXT,
                                  rate_point INT,
                                  FOREIGN KEY (user_id) REFERENCES users(id),
                                  FOREIGN KEY (res_id) REFERENCES restaurant(id)
);

CREATE TABLE orders (
                        id SERIAL PRIMARY KEY,
                        user_id INT,
                        res_id INT,
                        create_date TIMESTAMP,
                        FOREIGN KEY (user_id) REFERENCES users(id),
                        FOREIGN KEY (res_id) REFERENCES restaurant(id)
);

CREATE TABLE menu_restaurant (
                                cate_id INT,
                                res_id INT,
                                create_date TIMESTAMP,
                                PRIMARY KEY (cate_id, res_id),
                                FOREIGN KEY (cate_id) REFERENCES category(id),
                                FOREIGN KEY (res_id) REFERENCES restaurant(id)
);



CREATE TABLE promo (
                       id SERIAL PRIMARY KEY,
                       res_id INT,
                       percent INT,
                       start_date TIMESTAMP,
                       end_date TIMESTAMP,
                       FOREIGN KEY (res_id) REFERENCES restaurant(id)
);

CREATE TABLE order_item (
                           order_id INT,
                           food_id INT,
                           create_date TIMESTAMP,
                           PRIMARY KEY (order_id, food_id),
                           FOREIGN KEY (order_id) REFERENCES orders(id),
                           FOREIGN KEY (food_id) REFERENCES food(id)
);
INSERT INTO roles(role_name) VALUES('admin');

INSERT INTO roles(role_name) VALUES('user');

INSERT INTO roles(role_name) VALUES('guest');

INSERT INTO users(user_name,password,full_name,role_id, create_date)
VALUES('Vladislav','1234','Vladislav Petrov',1, CURRENT_TIMESTAMP);

INSERT INTO users(user_name,password,full_name,role_id, create_date)
VALUES('Roma','1234','Roma Petrov',2, CURRENT_TIMESTAMP);